#!/bin/bash
declare -a arrVar=()
 isComment='^[[:space:]]*#'
 isBlank='^[[:space:]]*$'
fileName=".env"

TYPE=$1

cat > ./infrastructure/parameter.go <<EOL
package infrastructure
func getStringEnvParameter(envParam string, defaultValue string) string {
	if value, ok := os.LookupEnv(envParam); ok {
		return value
	} else {
		return defaultValue
	}
}
EOL
echo "const("
while read line; do
    [[ $line =~ $isComment ]] && continue
    [[ $line =~ $isBlank ]] && continue
    key=$(echo "$line" | cut -d '=' -f 1)
    value=$(echo "$line" | cut -d '=' -f 2-)

    echo $key = "\"${key}\""| sed 's/ //g'
done <$fileName
echo ")"
echo "var("

while read line; do
    [[ $line =~ $isComment ]] && continue
    [[ $line =~ $isBlank ]] && continue
    key=$(echo "$line" | cut -d '=' -f 1)
    value=$(echo "$line" | cut -d '=' -f 2-)
    cv=$(echo "${key}"| sed 's/_/ /g')
    cvc=$(echo "${cv,,}")
    cvcv=$(echo "${cvc}"|sed -r 's/\<./\U&/g')
    cvcvc=$(echo "${cvcv}"| sed 's/ //g')
    echo "$cvcvc  string"
done <$fileName
echo ")"
echo "func loadEnvParameters() {"
while read line; do
    [[ $line =~ $isComment ]] && continue
    [[ $line =~ $isBlank ]] && continue
    key=$(echo "$line" | cut -d '=' -f 1)
    value=$(echo "$line" | cut -d '=' -f 2-)
    cv=$(echo "${key}"| sed 's/_/ /g')
    cvc=$(echo "${cv,,}")
    cvcv=$(echo "${cvc}"|sed -r 's/\<./\U&/g')
    cvcvc=$(echo "${cvcv}"| sed 's/ //g')
    vcv=$(echo "${value}"| sed 's/ //g')
    echo "$cvcvc  = getStringEnvParameter($key, \"$vcv\")"
done <$fileName
echo " }"



#for value in "${arrVar[@]}"
#do
#     echo $value
#done
