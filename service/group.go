package service

import (
	"errors"
	"ooad-support/model"
	"ooad-support/repository"
)

type GroupOption struct {
	repository.GroupQuery
}
type IGroupService interface {
	BaseService
	CreateGroup(group *model.Team) (*model.Team, error)
	GetListGroup(opt *GroupOption) (total uint, records []*model.Team, err error)
}

func NewGroupService(groupRepository repository.IGroupRepository) IGroupService {
	return &groupServiceIml{
		baseServiceIml:  NewBaseServiceIml(groupRepository),
		groupRepository: groupRepository,
	}
}

type groupServiceIml struct {
	baseServiceIml
	groupRepository repository.IGroupRepository
}

func (g *groupServiceIml) GetListGroup(opt *GroupOption) (total uint, records []*model.Team, err error) {
	total, records, err = g.groupRepository.GetListGroup(&opt.GroupQuery)
	if err != nil {
		g.ErrLog.Print(err)
		return 0, nil, err
	}
	return
}

func (g *groupServiceIml) CreateGroup(group *model.Team) (*model.Team, error) {
	// check
	isNotExsit, err := g.groupRepository.IsNotStudentInGroup(group.LeaderCode, group.CourseOfferingCode)
	if err != nil {
		g.ErrLog.Print(err)
		return nil, err
	}
	// createGroup
	if isNotExsit == false {
		return nil, errors.New("Bạn đã thuộc 1 nhóm khác")
	}
	group, err = g.groupRepository.CreateGroup(group)
	if err != nil {
		g.ErrLog.Print(err)
		return nil, err
	}
	return group, err

}