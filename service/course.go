package service

import (
	"ooad-support/infrastructure"
	"ooad-support/model"
	"ooad-support/repository"
)
type CourseOfferingOption struct {
	repository.CourseQuery
}
type ICourseService interface {
	BaseService
	GetListCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.CourseOffering, err error)
	GetStudentInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.StudentCourseResponse, err error)
	GetProjectInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.Project, err error)
	GetTeamsInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.TeamStudentResponse, err error)
}

func NewCourseService(courseRepository repository.ICourseRepository) ICourseService {
	return &courseServiceIml{
		baseServiceIml:    NewBaseServiceIml(courseRepository),
		courseRepository: courseRepository,
	}
}

type courseServiceIml struct {
	baseServiceIml
	courseRepository repository.ICourseRepository
}

func (c *courseServiceIml) GetListCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.CourseOffering, err error) {
	if opt != nil {

	}

	total, records, err = c.courseRepository.GetListCourseOffering(&opt.CourseQuery)
	if err != nil {
		infrastructure.ErrLog.Print(err)
		return 0, nil, err
	}

	return
}

func (c *courseServiceIml) GetStudentInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.StudentCourseResponse, err error) {
	total, records, err = c.courseRepository.GetStudentInCourseOffering(&opt.CourseQuery)
	if err != nil {

		infrastructure.ErrLog.Print(err)
	}

	return total, records, err
}

func (c *courseServiceIml) GetProjectInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.Project, err error) {
	panic("implement me")
}

func (c courseServiceIml) GetTeamsInCourseOffering(opt *CourseOfferingOption) (total uint, records []*model.TeamStudentResponse, err error) {
	panic("implement me")
}

