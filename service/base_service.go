package service

import (
	"ooad-support/helper"
	"ooad-support/infrastructure"
	"ooad-support/repository"
)
type BaseService interface {
	Create(model interface{}) (interface{}, error)
	UpdateOne(id string, valude interface{}, model interface{}) (interface{}, error)
	FindOne(id string, valude interface{}, model interface{}) (interface{}, error)
}

func NewBaseService(baseRepository repository.BaseRepository) BaseService {
	return &baseServiceIml{
		baseRepository: baseRepository,
	}
}
func NewBaseServiceIml(baseRepository repository.BaseRepository) baseServiceIml {
	s := baseServiceIml{
		Logger:         infrastructure.NewLogger(),
		baseRepository: baseRepository,
	}
	s.InfoLog.SetPrefix("\033[32m[SERVICE]\033[0m " + s.InfoLog.Prefix())
	s.ErrLog.SetPrefix("\033[32m[SERVICE]\033[0m " + s.ErrLog.Prefix())
	s.InfoLog.Print(s.baseRepository)
	return s
}
type baseServiceIml struct {
	infrastructure.Logger
	baseRepository repository.BaseRepository
}

func (b *baseServiceIml) Create(model interface{}) (interface{}, error) {

	b.InfoLog.Print(helper.GetNameFunctionCall())

	if err := b.baseRepository.CreateOne(model); err != nil {
		b.ErrLog.Print(err)
		return nil, err
	}
	return model, nil

}

func (b *baseServiceIml) UpdateOne(id string, val interface{}, model interface{}) (interface{}, error) {
	if err := b.baseRepository.UpdateOne(id, val, model); err != nil {
		infrastructure.ErrLog.Print(err)
		return nil, err
	}
	return model, nil
}

func (b *baseServiceIml) FindOne(id string, val interface{}, model interface{}) (interface{}, error) {
	if err := b.baseRepository.FindOne(id, val, model); err != nil {
		infrastructure.ErrLog.Print(err)
		return nil, err
	}
	return model, nil
}
