package service

import (
	"ooad-support/model"
	"ooad-support/repository"
)

type TopicOption struct {
	repository.TopicQuery
}
type ITopicService interface {
	GetListTopic(TopicOption) (total uint, records []*model.Project, err error)
	BaseService
}

func NewTopicService(projectRepository repository.ITopicRepository) ITopicService {

	return &topicServiceIml{
		baseServiceIml:    NewBaseServiceIml(projectRepository),
		projectRepository: projectRepository,
	}
}

type topicServiceIml struct {
	baseServiceIml
	projectRepository repository.ITopicRepository
}

func (t *topicServiceIml) GetListTopic(option TopicOption) (total uint, records []*model.Project, err error) {
	if total, records, err = t.projectRepository.GetListProject(option.TopicQuery); err != nil {
		t.ErrLog.Print(err)
		return 0, nil, err

	}
	return
}
