module ooad-support

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-openapi/spec v0.20.2 // indirect
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gobeam/stringy v0.0.0-20200717095810-8a3637503f62
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo/v4 v4.1.17
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/swaggo/echo-swagger v1.1.0
	github.com/swaggo/swag v1.7.0
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/tools v0.1.0 // indirect
)
