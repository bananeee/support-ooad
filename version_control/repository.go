package version_control

import (
	"ooad-support/model"
	"ooad-support/repository"
	"ooad-support/repository/postgre"
)

var courseReposirory repository.ICourseRepository
var groupRepository repository.IGroupRepository
var projectRepository repository.ITopicRepository

func CreateRepository() {
	course := model.CourseOffering{}
	courseReposirory = postgre.NewCourseRepository(course.TableName(), GetPostgreConnection())
	group := model.Team{}
	groupRepository = postgre.NewGroupRepository(group.TableName(), GetPostgreConnection())
	topic := model.Project{}
	projectRepository = postgre.NewTopicRepository(topic.TableName(), GetPostgreConnection())

}
func getCourseRepositoy() repository.ICourseRepository {
	return courseReposirory
}
