package version_control

import "ooad-support/service"

var courseService service.ICourseService
var projectService service.ITopicService
var groupService service.IGroupService

func CreateService() {
	courseService = service.NewCourseService(courseReposirory)
	projectService = service.NewTopicService(projectRepository)
	groupService = service.NewGroupService(groupRepository)
}
func GetCourseService() service.ICourseService {
	return courseService
}
func GetProjectService() service.ITopicService {
	return projectService
}
func GetGroupService()  service.IGroupService {
	return  groupService
}
