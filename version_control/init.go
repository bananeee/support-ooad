package version_control

import (
	"github.com/jinzhu/gorm"
	"ooad-support/adapter"
	"ooad-support/infrastructure"
)

var ProsgresConnection *gorm.DB

func init() {
	var err error
	ProsgresConnection, err = adapter.PostgreSQLOpenConnection(infrastructure.PostgreHost, infrastructure.PostgrePort, infrastructure.PostgreDbUser, infrastructure.PostgreDbPassword, infrastructure.PostgreDbName)
	if err != nil {
		infrastructure.ErrLog.Print(err)
	}
	CreateRepository()
	CreateService()
}
func GetPostgreConnection() *gorm.DB {
	return ProsgresConnection
}
