package controller

import (
	"net/http"
	"ooad-support/controller/payload"
	"ooad-support/controller/query"
	"ooad-support/controller/response"
	"ooad-support/model"
	"ooad-support/service"
	"ooad-support/version_control"
	"strconv"

	"github.com/labstack/echo/v4"
)

type IProjectController interface {
	//function interface

	GetListProject(c echo.Context) error

	GetProject(c echo.Context) error

	UpdateProject(c echo.Context) error

	CreateProject(c echo.Context) error
}

func NewProjectController() IProjectController {
	return &projectControllerIml{
		baseController: NewBaseController(),
		projectService: version_control.GetProjectService(),
	}

}

type projectControllerIml struct {
	*baseController
	projectService service.ITopicService
}

//function implement

func (p *projectControllerIml) GetListProject(c echo.Context) error {

	res := new(response.Response)
	param := new(query.GetListProjectQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	opt := service.TopicOption{}
	opt.CourseOfferingCode = param.CourseCode
	_, data, err := p.projectService.GetListTopic(opt)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}

func (p *projectControllerIml) GetProject(c echo.Context) error {

	res := new(response.Response)
	param := new(query.GetProjectQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	project := new(model.Project)
	data, err := p.projectService.FindOne("id", param.ProjectID, project)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}

func (p *projectControllerIml) UpdateProject(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.UpdateProjectPayload)
	//param := new(query.UpdateProjectQuery)

	if err := c.Bind(body); err != nil {
		p.InfoLog.Print(err)
		return err
	}
	//if err := c.Bind(param); err != nil {
	//	p.InfoLog.Print(err)
	//	return err
	//}
	id, _ := strconv.Atoi(c.Param("project_id"))
	if err := c.Validate(body); err != nil {
		p.InfoLog.Print(err)
		return err
	}

	project := new(model.Project)
	project.BaseProject = body.BaseProject

	data, err := p.projectService.UpdateOne("id", id, project)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)

	return c.JSON(http.StatusOK, res)
}

func (p *projectControllerIml) CreateProject(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.CreateProjectPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	project := new(model.Project)
	project.BaseProject = body.BaseProject
	data, err := p.projectService.Create(project)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}
