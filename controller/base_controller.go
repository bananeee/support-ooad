package controller

import "ooad-support/infrastructure"

type baseController struct {
	infrastructure.Logger
}

func NewBaseController() *baseController {
	baseRepo := baseController{

		Logger: infrastructure.NewLogger(),
	}
	baseRepo.ErrLog.SetPrefix("\033[32m[CONTROLLER]\033[0m " + baseRepo.ErrLog.Prefix())
	baseRepo.InfoLog.SetPrefix("\033[32m[CONTROLLER]\033[0m " + baseRepo.InfoLog.Prefix())
	return &baseRepo
}