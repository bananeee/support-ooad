package response

type Response struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewResponse(data interface{}) *Response {
	return &Response{
		Success: true,
		Message: "ok",
		Data:    data,
	}
}
