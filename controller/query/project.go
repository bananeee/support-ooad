package query

//gen query
type GetListgroupQuery struct {
}

type GetListProjectQuery struct {
	CourseCode string `query:"course_code" swaggertype:"string" json:"course_code"`
}
type GetProjectQuery struct {
	ProjectID uint `param:"project_id" swaggertype:"integer"`
}
type UpdateProjectQuery struct {
	ProjectID uint `param:"project_id" swaggertype:"integer" json:"project_id"`
}
