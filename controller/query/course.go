package query

//gen query
type GetMyCourseQuery struct {
}
type GetProjectInCourseQuery struct {
	CourseCode string `param:"course_code" swaggertype:"string"`
}
type GetGroupInCourseQuery struct {
	CourseCode string `param:"course_code" swaggertype:"string"`
}
type GetStudentInCourseQuery struct {
	CourseCode string `param:"course_code" swaggertype:"string"`
}
