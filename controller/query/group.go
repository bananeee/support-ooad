package query

//gen query
 type GetListGroupQuery struct {
	 CourseCode string `query:"course_code" swaggertype:"string"`
 }

type UpdateGroupQuery struct {
	GroupID uint `param:"group_id" swaggertype:"integer"`
}

