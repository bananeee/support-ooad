package controller

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"ooad-support/controller/query"
	"ooad-support/controller/response"
	"ooad-support/helper"
	"ooad-support/service"
	"ooad-support/version_control"
)

const teacherRole = "teacher"
const studentRole = "student"

type ICourseController interface {
	//function interface
	GetMyCourse(c echo.Context) error

	GetProjectInCourse(c echo.Context) error

	GetGroupInCourse(c echo.Context) error

	GetStudentInCourse(c echo.Context) error
}

func NewCourseController() ICourseController {

	return &courseControllerIml{
		baseController: NewBaseController(),
		courseService:  version_control.GetCourseService(),
	}
}

type courseControllerIml struct {
	*baseController
	courseService service.ICourseService
}

//function implement
func (courseController *courseControllerIml) GetMyCourse(c echo.Context) error {
	helper.GetNameFunctionCall()
	token := c.Request().Header.Get("authorization")
	user, err := helper.GetUser(token)
	if err != nil {
		courseController.ErrLog.Print(err)
		return err
	}
	res := new(response.Response)
	param := new(query.GetMyCourseQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	// service
	var opt service.CourseOfferingOption
	if user.Role == teacherRole {
		opt.TeacherCode = user.Code
	}
	if user.Role == studentRole {
		opt.StudentCode = user.Code
	}
	_, data, err := courseController.courseService.GetListCourseOffering(&opt);
	if err!= nil{
		return err
	}
	res= response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}

func (courseController *courseControllerIml) GetProjectInCourse(c echo.Context) error {
	helper.GetNameFunctionCall()
	token := c.Request().Header.Get("authorization")
	user, err := helper.GetUser(token)
	if err != nil {
		courseController.ErrLog.Print(err)
		return err
	}
	courseController.InfoLog.Print(user)
	res := new(response.Response)
	param := new(query.GetProjectInCourseQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	// service
	var opt service.CourseOfferingOption
	opt.CourseCode = param.CourseCode

	_, data, err := courseController.courseService.GetStudentInCourseOffering(&opt)
	if err != nil {
		return err
	}

	res.Data = data
	return c.JSON(http.StatusOK, res)
}

func (courseController *courseControllerIml) GetGroupInCourse(c echo.Context) error {

	res := new(response.Response)
	param := new(query.GetGroupInCourseQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, res)
}

func (courseController *courseControllerIml) GetStudentInCourse(c echo.Context) error {

	res := new(response.Response)
	param := new(query.GetStudentInCourseQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	opt := service.CourseOfferingOption{
	}
	opt.CourseCode = param.CourseCode
	_, data, err := courseController.courseService.GetStudentInCourseOffering(&opt)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}
