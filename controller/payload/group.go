package payload

import "ooad-support/model"

//gen payload
 type RejectMemberInGroupPayload struct {
  GroupID uint `json:"group_id"`
  StudentID uint `json:"student_id"`
}
 type AcceptMemberInGroupPayload struct {
  GroupID uint `json:"group_id"`
  StudentID uint `json:"student_id"`
}
 type AddMemberInGroupPayload struct {
	GroupID uint `json:"group_id"`
}
 type UpdateGroupPayload struct {
  model.BaseGroup
}
 type CreateGroupPayload struct {
	model.BaseGroup
}