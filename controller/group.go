package controller

import (
	"errors"
	"net/http"
	"ooad-support/controller/payload"
	"ooad-support/controller/query"
	"ooad-support/controller/response"
	"ooad-support/helper"
	"ooad-support/model"
	"ooad-support/service"
	"ooad-support/version_control"

	"github.com/labstack/echo/v4"
)

type IGroupController interface {
	//function interface
	GetListGroup(c echo.Context) error

	RejectMemberInGroup(c echo.Context) error

	AcceptMemberInGroup(c echo.Context) error

	AddMemberInGroup(c echo.Context) error

	UpdateGroup(c echo.Context) error

	CreateGroup(c echo.Context) error
}

func NewGroupController() IGroupController {
	return &groupControllerIml{
		groupService: version_control.GetGroupService(),
	}
}

type groupControllerIml struct {
	groupService service.IGroupService
}

//function implement
func (g *groupControllerIml) GetListGroup(c echo.Context) error {

	res := new(response.Response)
	param := new(query.GetListGroupQuery)
	if err := c.Bind(param); err != nil {
		return err
	}
	if err := c.Validate(param); err != nil {
		return err
	}
	opt:= service.GroupOption{}
	opt.CourseCode= param.CourseCode
	_,data, err:=g.groupService.GetListGroup(&opt)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}

func (g *groupControllerIml) RejectMemberInGroup(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.RejectMemberInGroupPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, res)
}

func (g *groupControllerIml) AcceptMemberInGroup(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.AcceptMemberInGroupPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, res)
}

func (g *groupControllerIml) AddMemberInGroup(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.AddMemberInGroupPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, res)
}

func (g *groupControllerIml) UpdateGroup(c echo.Context) error {
	res := new(response.Response)
	body := new(payload.UpdateGroupPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	group := new(model.Team)
	group.BaseGroup = body.BaseGroup
	data, err := g.groupService.Create(group)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}

func (g *groupControllerIml) CreateGroup(c echo.Context) error {
	token := c.Request().Header.Get("authorization")
	user, err := helper.GetUser(token)
	if err != nil {
		return err
	}
	if user.Role != "student" {
		return errors.New("not role")
	}
	res := new(response.Response)
	body := new(payload.CreateGroupPayload)
	if err := c.Bind(body); err != nil {
		return err
	}
	if err := c.Validate(body); err != nil {
		return err
	}
	group := new(model.Team)
	group.BaseGroup = body.BaseGroup
	group.LeaderCode = user.Code
	data, err := g.groupService.CreateGroup(group)
	if err != nil {
		return err
	}
	res = response.NewResponse(data)
	return c.JSON(http.StatusOK, res)
}
