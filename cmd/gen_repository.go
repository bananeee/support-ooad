package main

import (
	"fmt"
	"github.com/gobeam/stringy"
	"io/ioutil"
	"log"
	"os"
	"unicode"
)

func genRepository(nameFile string) {
	str := stringy.New(nameFile)
	result := str.CamelCase("?", "")
	log.Printf(result)
	template := `package repository

type %sQuery struct {
	
}
type I%sRepository interface {
	BaseRepository
}
`
	template = fmt.Sprintf(template, result, result)
	if ExistFiles(fmt.Sprintf("./repository/postgre/%s.go", nameFile)){
		log.Print("file đã tồn tai")
		return
	}
	err := ioutil.WriteFile(fmt.Sprintf("./repository/%s.go", nameFile), []byte(template), 0644)
	log.Print(err)

}
func genRepositoryIml(nameFile string) {
	if ExistFiles(fmt.Sprintf("./repository/postgre/%s.go", nameFile)){
		log.Print("file đã tồn tai")
		return
	}
	str := stringy.New(nameFile)
	result := str.CamelCase("?", "")
	log.Printf(result)
	template := `package postgre

type %sRepositoryIml struct {
	baseRepository
}

func New%sRepository(tableName string, db *gorm.DB) repository.I%sRepository  {
	return &%sRepositoryIml{
		baseRepository{
			DB:    db,
			Table: db.Table(tableName),
		},
	}
}
`
	template = fmt.Sprintf(template, LowCaseFirstCharacter(nameFile), result, result, LowCaseFirstCharacter(nameFile))
	err := ioutil.WriteFile(fmt.Sprintf("./repository/postgre/%s.go", nameFile), []byte(template), 0644)
	log.Print(err)

}
func ExistFiles(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
func LowCaseFirstCharacter(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}
func main() {
	genRepository(os.Args[1])
	genRepositoryIml(os.Args[1])
}
