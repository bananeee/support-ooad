package main

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var InfoLog = log.New(os.Stdout, "\u001B[1;34m[INFO]\u001B[0m ", log.Ldate|log.Ltime|log.Llongfile)
const 	packageName = "package main \n \n"
type Router struct {
	Method string `json:"method"`
	Path   string `json:"path"`
	Name   string `json:"name"`
}

func (r *Router) getFunctionName() string {
	funcName := strings.Replace(r.Name, "-fm", "", 1)
	lastSlash := strings.LastIndexByte(funcName, '/')
	if lastSlash < 0 {
		lastSlash = 0
	}
	lastDot := strings.LastIndexByte(funcName[lastSlash:], '.') + lastSlash
	return funcName[lastDot+1:]
}
func (r *Router) getNamePayload() string {
	return r.getFunctionName() + "Payload"
}
func (r *Router) getNameQuery() string {

	return r.getFunctionName() + "Query"
}
func (r *Router) getNameParam() string {
	return r.getFunctionName() + "Param"
}

func genSwaggerComment() {
	// get file
	data, err := getFileRouter()
	if err != nil {
		return
	}
	// get string
	var comment string =packageName
	for i, item := range data {
		comment+=genOneRouter(item,i)
	}
	ioutil.WriteFile("swagger.go", []byte(comment), 0644)
	// write file

}
func getFileRouter() ([]*Router, error) {
	data, err := ioutil.ReadFile("routes.json")
	if err != nil {
		return nil, err
	}
	units := []*Router{}
	err = json.Unmarshal(data, &units)
	return units, err
}

func genOneRouter(router *Router,i int) string {
	// get
	template :=  "// @Summary  " + router.getFunctionName() + "\n" +
		"// @Description ádasd\n" +
		"// @Id\n" +
		"// @Accept json\n" +
		"// @Produce json\n" +
		"// @Security ApiKeyAuth\n"
	// get payload if exist
	template += genPayload(router.getNamePayload())
	InfoLog.Print(template)
	// get query and param if exist
	template +=genQuery(router.getNameQuery())

	// response
	template+="// @Success 200 {object} response.Response  \"desc\" \n"
	// router
	template += "// @Router " + replacePath(router.Path) +" ["+router.Method+"]\n"
	// function default
	template+="func DefaultFunction"+strconv.Itoa(i)+"()  {}\n \n \n \n"

	InfoLog.Print(template)
	return template
}
func genPayload(name string) string {
	if findStructInPackagePayload(name) {

		return fmt.Sprintf("// @Param payload body payload.%s true \"information\" \n", name)
	}
	return ""
}
func genQuery(name string) string {
	tagFields:=findStructInPackage(name)
	allQuery := ""
	for _,tagField := range tagFields{
		tagVal:=getTagToMap(tagField)
		nameQuery := tagVal["query"]
		if nameQuery == "" {

		}else{
			typeQuery := tagVal["swaggertype"]
			teamplate := "// @Param " + nameQuery + " query " + typeQuery + " false \"count\"  "
			if tagVal["collectionFormat"]!=""{
				teamplate+=  tagVal["collectionFormat"] +" \n"
			}else{
				teamplate+=  " \n"
			}
			allQuery = allQuery + teamplate
		}
		nameParam := tagVal["param"]
		if nameParam == "" {

		}else{
			typeQuery := tagVal["swaggertype"]
			teamplate := "// @Param " + nameParam + " path " + typeQuery + " false \"count\"  "
			if tagVal["collectionFormat"]!=""{
				teamplate+=  tagVal["collectionFormat"] +" \n"
			}else{
				teamplate+=  " \n"
			}
			allQuery = allQuery + teamplate
		}

	}
	return allQuery
}
func visit(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}
		if !info.IsDir() {
			*files = append(*files, path)
		}

		return nil
	}
}
func getFileInPackage(path string) []string {
	var files []string
	err := filepath.Walk(path, visit(&files))
	if err != nil {
		panic(err)
	}
	return files
}
func getTagToMap(tagField string) map[string]string {
	tagField = strings.ReplaceAll(tagField, "`", "")
	tagField = strings.TrimSpace(tagField)
	tagField = strings.Join(strings.Fields(strings.TrimSpace(tagField)), " ")
	tags := strings.Split(tagField, " ")
	m := make(map[string]string)
	for _, pair := range tags {
		z := strings.Split(pair, ":")
		m[z[0]] = strings.ReplaceAll(z[1], "\"", "")
	}
	return m

}
func replacePath(path string) string {
	abc := strings.Split(path, "/")
	newPath := ""
	for _, s := range abc {
		if strings.Contains(s, ":") {
			s = strings.ReplaceAll(s, ":", "")
			s = "{" + s + "}"
		}
		if s != "" {
			s = "/" + s
		}

		newPath += s
	}
	return newPath
}
func findStructInPackagePayload(name string) bool {
	files := getFileInPackage("./controller/payload")
	var structType *ast.TypeSpec
	for _, file := range files {
		structType = findStructInFile(name, file)
		if structType != nil {
			return  true
		}
	}
	return false
}
func findStructInPackage(name string) []string {
	files := getFileInPackage("./controller/query")
	var structType *ast.TypeSpec
	for _, file := range files {
		structType = findStructInFile(name, file)
		if structType != nil {
			break
		}
	}
	return getAllFieldTag(structType)
}
func getAllFieldTag( structType *ast.TypeSpec ) []string {
	tagField := []string{}
	if structType == nil {
		return []string{}
	}
	structDecl := structType.Type.(*ast.StructType)
	for _, fld := range structDecl.Fields.List {
		// get fld.Type as string
		if fld.Tag != nil {
			tagField = append(tagField, fld.Tag.Value)
		}
	}
	return tagField
}
func findStructInFile(name string, path string) *ast.TypeSpec {
	fset := token.NewFileSet() // positions are relative to fset
	f, err := parser.ParseFile(fset, path, nil, 0)
	if err != nil {
		panic(err)
	}

	for _, node := range f.Decls {
		switch node.(type) {

		case *ast.GenDecl:
			genDecl := node.(*ast.GenDecl)
			for _, spec := range genDecl.Specs {
				switch spec.(type) {
				case *ast.TypeSpec:
					typeSpec := spec.(*ast.TypeSpec)

					fmt.Printf("Struct: name=%s\n", typeSpec.Name.Name)
					if typeSpec.Name.Name == name {
						return typeSpec
					}

				}
			}
		}
	}
	return nil
}
func main() {
	genSwaggerComment()
}
