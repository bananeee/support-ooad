package main

import (
	"ooad-support/cmd/common"
	"fmt"
	"github.com/gobeam/stringy"
	"io/ioutil"
	"log"
	"os"
)

func genService(nameFile string) {
	str := stringy.New(nameFile)
	result := str.CamelCase("?", "")
	log.Printf(result)
	if common.ExistFiles(fmt.Sprintf("./service/%s.go", nameFile)){
		log.Print("file đã tồn tai ")
		return
	}
	template := `package service
  


type I%sService interface {
	BaseService
}

func New%sService() I%sService {
	return &%sServiceIml{
		baseServiceIml:    NewBaseServiceIml(nil),
	}
}

type %sServiceIml struct {
	baseServiceIml
}
`
	template = fmt.Sprintf(template, result,result,result,common.LowCaseFirstCharacter(nameFile),common.LowCaseFirstCharacter(nameFile))
	ioutil.WriteFile(fmt.Sprintf("./service/%s.go", nameFile), []byte(template), 0644)

}
func main()  {
	genService(os.Args[1])
}