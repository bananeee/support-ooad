package main

import (
	"fmt"
	"github.com/gobeam/stringy"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"unicode"
)

func createController(nameFile string) {
	// check file exist
	path := "./controller/" + nameFile + ".go"
	if Exists(path) {
		log.Print("file đã tồn tại")
		return
	}
	genFileController(nameFile)
	// gen file controller

	// gen file payload
	genFilePayload(nameFile)
	// gen file param
	genFileParam(nameFile)
}

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func ToSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}
func LcFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}
func genFileController(nameFile string) {
	str := stringy.New(nameFile)
	result := str.CamelCase("?", "")
	log.Printf(result)
	template := `package controller

import "github.com/labstack/echo/v4"
type I%sController interface {
//function interface

}
type %sControllerIml struct {
}

//function implement
`
	template = fmt.Sprintf(template, result, LcFirst(result))
	ioutil.WriteFile(fmt.Sprintf("./controller/%s.go", nameFile), []byte(template), 0644)

}
func genFileParam(nameFile string) {
	template := `package query
//gen query`
	ioutil.WriteFile(fmt.Sprintf("./controller/query/%s.go", nameFile), []byte(template), 0644)
}
func genFilePayload(nameFile string) {
	template := `package payload
//gen payload`
	err:=ioutil.WriteFile(fmt.Sprintf("./controller/payload/%s.go", nameFile), []byte(template), 0644)
	log.Print(err)
}

func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
func main() {

	createController(os.Args[1])
}
