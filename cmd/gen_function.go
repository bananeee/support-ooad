package main

import (
	"fmt"
	"github.com/gobeam/stringy"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func getInterFaceFunction(name string) string {
	return fmt.Sprintf("%s(c echo.Context) error \n", name)

}
func getImplementFunction(implementStruct, name string,hasBody int ,hasParam int) string {
	template:="\n\tres :=new(response.Response)\n"
	if hasBody==1{
		template= fmt.Sprintf("%s\tbody := new(payload.%sPayload)\n\tif err := c.Bind(body); err != nil {\n\t\treturn err\n\t}\n\tif err := c.Validate(body); err != nil {\n\t\treturn err\n\t}",template,name)
	}
	if hasParam==1{
		template=fmt.Sprintf("\n%s\tparam:= new( query.%sQuery)\n\tif err := c.Bind(param); err != nil {\n\t\treturn err\n\t}\n\tif err := c.Validate(param); err != nil {\n\t\treturn err\n\t}",template,name)

	}
	template+="\n\treturn c.JSON(http.StatusOK, res)\n"
	log.Print(template)

	return fmt.Sprintf("func (%c *%s)%s(c echo.Context) error{%s} \n", implementStruct[0], implementStruct, name,template)
}
func genPayloadStruct(path string, interfaceFunction string) {
	template := `type %sPayload struct {
	
}`
	input, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {
		if strings.Contains(line, "gen payload") {
			lines[i] = fmt.Sprintf("//gen payload\n %s", fmt.Sprintf(template, interfaceFunction))
			break
		}
	}
	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(path, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
func genParamStruct(path string, interfaceFunction string) {
	template := `type %sQuery struct {
	
}`
	input, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {
		if strings.Contains(line, "gen query") {
			lines[i] = fmt.Sprintf("//gen query\n %s", fmt.Sprintf(template, interfaceFunction))
			break
		}
	}
	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(path, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
func genControllerFunction(path string, interfaceFunction string, implementFunction string) {
	input, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {
		if strings.Contains(line, "function interface") {
			lines[i] = fmt.Sprintf("//function interface\n %s", interfaceFunction)
			break
		}
	}
	for i, line := range lines {
		if strings.Contains(line, "function implement") {
			lines[i] = fmt.Sprintf("//function implement\n %s", implementFunction)
			break
		}
	}
	output := strings.Join(lines, "\n")
	err = ioutil.WriteFile(path, []byte(output), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
func LowCaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}
func genAllFunction(functionName, fileName string, hasPayload int, hasParam int) {
	if hasPayload == 1 {
		genPayloadStruct(fmt.Sprintf("controller/payload/%s.go", fileName), functionName)
	}

	if hasParam == 1 {
		genParamStruct(fmt.Sprintf("controller/query/%s.go", fileName), functionName)
	}
	str := stringy.New(fileName)
	result := str.CamelCase("?", "")
	genControllerFunction(fmt.Sprintf("controller/%s.go", fileName), getInterFaceFunction(functionName), getImplementFunction(LowCaseFirst(fmt.Sprintf("%sControllerIml", result)), functionName,hasPayload,hasParam))

}

func main() {
	log.Printf(os.Args[1])
	//n := flag.String("n", "", "Text to parse. (Required)")
	//f := flag.String("f", "chars", "Metric {chars|words|lines};. (Required)")
	//p := flag.Int("p", 0, "Measure unique values of a metric.")
	//q := flag.Int("q", 0, "Measure unique values of a metric.")
	//flag.Parse()
	param,_:=strconv.Atoi(os.Args[3])
	payload,_:=strconv.Atoi(os.Args[4])
	genAllFunction(os.Args[1], os.Args[2], param, payload)
}
