package adapter

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"time"
)

func PostgreSQLOpenConnection(dBHost string, dbPort string, dBUsername string, DbPassword string, dBName string, ) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", "host="+dBHost+" port="+dbPort+" user="+dBUsername+" dbname="+dBName+" password="+DbPassword+" sslmode=disable")
	//defer db.Close()
	if err != nil {
		return nil, err
	}

	db.LogMode(true)
	//SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	db.DB().SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	db.DB().SetMaxOpenConns(100)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	db.DB().SetConnMaxLifetime(time.Hour)
	return db, nil
}
