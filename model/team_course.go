package model

type TeamCourse struct {
	ID             uint   `json:"id" gorm:"auto_increment"`
	TeamID         int    `json:"team_id"`
	CourseOffering string `json:"student_code"`
	Status         bool
}
