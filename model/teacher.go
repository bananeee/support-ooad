package model

type Teacher struct {
	Code   string  ` json:"code" gorm:"primary_key"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

func (Teacher) TableName() string {
	return "teachers"
}
