package model

type UseCaseSlice struct {
	ID          uint
	Order       uint   `json:"order"`
	Description string `json:"description"`
	StudentCode string
	Assignee    Student `json:"assignee"`
	UseCaseID   uint
	IterationID uint
}
type Mark struct {
	ID             uint
	UseCaseSliceID uint
	StudentCode    string
}
