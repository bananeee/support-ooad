package model

type StudentCourse struct {
	ID                 uint    `json:"id" gorm:"auto_increment"`
	StudentCode        string  `json:"student_code" `
	CourseOfferingCode string  `json:"course_offering"`
	Total              float64 `json:"total"`
	Status             bool
}

func (StudentCourse) TableName() string {
	return "student_courses"
}

type StudentCourseResponse struct {
	StudentCode        string  `json:"student_code" `
	Name   string `json:"name"`
	SchoolYear string `json:"school_year"`
	Avatar string `json:"avatar"`
	Total              float64 `json:"total"`
	TeamName string `json:"team_name"`
	TeamCode string `json:"team_code"`
}
