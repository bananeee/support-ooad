package model

type User struct {
	Code string `json:"code"`
	Role string `json:"role"`
}
