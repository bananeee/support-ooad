package model

type Project struct {
	ID int `json:"id" gorm:"auto_increment"`
	BaseProject
	CreatedBy     string `json:"create_by"`
	Status        bool   `json:"status"`
	OwnedByTeamID *int   `json:"-"`
	OwnedByTeam   *Team  `json:"owned_by_team"  gorm:"foreign_key:owned_by_team_id"`
}
type BaseProject struct {
	CourseCode       string `json:"course_code"`
	Name             string `json:"name"`
	ShortDescription string `json:"short_description"`
	Description      string `json:"description"`
	State            string `json:"state"`
	GlossaryOfTerms  string `json:"glossary_of_terms"`
}

func (Project) TableName() string {
	return "projects"
}
