package model

type Student struct {
	Code   string ` json:"code" gorm:"primary_key"`
	Name   string `json:"name"`
	SchoolYear string `json:"school_year"`
	Avatar string `json:"avatar"`
}

func (Student) TableName() string {
	return "students"
}
