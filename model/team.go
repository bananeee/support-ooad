package model

type BaseGroup struct {
	Name               string `json:"name"`
	CourseOfferingCode string `json:"course_code"`
}
type Team struct {
	ID int `json:"id" gorm:"auto_increment"`
	BaseGroup
	LeaderCode     string         `json:"-"`
	Leader         Student        `json:"leader" gorm:"foreign_key:leader_code"`
	CourseOffering CourseOffering `json:"course_offering" gorm:"foreign_key:course_offering_code"`
	Members        []Student `json:"members" gorm:"many2many:team_students;foreignKey:ID;joinForeignKey:TeamID;References:StudentCode;JoinReferences:Code"`
}

func (Team) TableName() string {
	return "teams"
}

type TeamStudent struct {
	ID          uint   `json:"id" gorm:"auto_increment"`
	TeamID      int    `json:"team_id" `
	StudentCode string `json:"student_code"`
	CourseCode  string `json:"course_code"`
	Status      bool
}
type TeamStudentResponse struct {
	StudentCode string `json:"student_code"`
	StudentName string `json:"student_name"`
	TeamID      int `json:"team_id"`
	LeaderCode  string `json:"leader_code"`
	ProjectID   string `json:"project_id"`
	TeamName    string `json:"team_name"`
	ProjectName string `json:"project_name"`
}
