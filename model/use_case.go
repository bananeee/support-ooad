package model

type UseCase struct {
	ID                 uint
	ProjectID          uint
	Order              uint   `json:"order" bson:"order"`
	Name               string `json:"name" bson:"name"`
	Actor              string `json:"actor" bson:"actor"`
	Priority           string `json:"priority"`
	Description        string `json:"description"`
	SpecialRequirement string `json:"special_requirement" bson:"special_requirement"`
	PreCondition       string `json:"pre_condition" bson:"pre_condition"`
	PostCondition      string `json:"post_condition" bson:"post_condition"`
	BasicFlow          string `json:"basic_flow" bson:"basic_flow"`
	AlternativeFlow    string `json:"alternative_flow" bson:"alternative_flow"`
	ExtensionPoints    string `json:"extension_points" bson:"extension_points"`
	Status             string
}
type UseCaseToUseCase struct {
	FromUseCaseID uint
	ToUseCaseID   uint
	Condition     string
}
type UseCaseToFlowStep struct {
	FromFlowStepID uint
	ToUseCaseID    uint
}
type UseCaseToUseCaseStory struct {
	UseCaseStoryID uint
	UseCaseID      uint
}
