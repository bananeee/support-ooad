package model

type UseCaseStory struct {
	ID          uint
	Name        string
	UseCaseID   uint
	Description string
	IsBasicFlow bool
}
type UseCaseStoryToFlowStep struct {
	UseCaseStoryID uint
	FlowStepID     uint
}
