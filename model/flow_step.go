package model

type FlowStep struct {
	ID          uint
	Description string
}
type FlowStepToFlowStep struct {
	UseCaseStoryID uint
	FromFlowStepID uint
	ToFlowStepID   uint
}
