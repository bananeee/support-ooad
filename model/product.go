package model

type BaseProduct struct {
	Name string `json:"name"`
}
type Product struct {
	BaseProduct
}

func (Product) TableName() string {
	return "products"
}
func NewProduct(base BaseProduct) *Product {
	return &Product{
		BaseProduct: base,
	}
}
