package model

type CourseOffering struct {
	Code        string   `json:"code" gorm:"primary_key"`
	Name        string   `json:"name"`
	Location    string   `json:"location"`
	StartLesson uint     `json:"start_lesson"`
	EndLesson   uint     `json:"end_lesson"`
	SchoolYear  string   `json:"school_year"`
	Weekday     string   `json:"weekday"`
	Serial      string   `json:"serial"`
	TeacherCode string   `json:"-"`
	Status      string   `json:"status"`
	Teacher     *Teacher `json:"teacher" gorm:"foreign_key:teacher_code"`
}

func (CourseOffering) TableName() string {
	return "course_offerings"
}
