package model

import "time"

type Iteration struct {
	ID          uint
	ProjectID   uint
	Description string
	StartTime   time.Time
	EnTime      time.Time
}
