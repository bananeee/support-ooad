package infrastructure

import "os"

func getStringEnvParameter(envParam string, defaultValue string) string {
	if value, ok := os.LookupEnv(envParam); ok {
		return value
	} else {
		return defaultValue
	}
}
const (
	POSTGRE_HOST        = "POSTGRE_HOST"
	POSTGRE_PORT        = "POSTGRE_PORT"
	POSTGRE_DB_NAME     = "POSTGRE_DB_NAME"
	POSTGRE_DB_USER     = "POSTGRE_DB_USER"
	POSTGRE_DB_PASSWORD = "POSTGRE_DB_PASSWORD"
)

var (
	PostgreHost       string
	PostgrePort       string
	PostgreDbName     string
	PostgreDbUser     string
	PostgreDbPassword string
)
func loadEnvParameters() {
	PostgreHost = getStringEnvParameter(POSTGRE_HOST, "149.28.157.34")
	PostgrePort = getStringEnvParameter(POSTGRE_PORT, "9001")
	PostgreDbName = getStringEnvParameter(POSTGRE_DB_NAME, "dev_ooad")
	PostgreDbUser = getStringEnvParameter(POSTGRE_DB_USER, "postgres")
	PostgreDbPassword = getStringEnvParameter(POSTGRE_DB_PASSWORD, "13081999")
}
