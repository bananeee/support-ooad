package infrastructure

import (
	"log"
	"os"
)

type Logger struct {
	InfoLog *log.Logger
	ErrLog  *log.Logger
}

func NewLogger() Logger {
	return Logger{
		InfoLog:  log.New(os.Stdout, "\u001B[1;34m[INFO]\u001B[0m ", log.Ldate|log.Ltime|log.Llongfile),
		ErrLog:  log.New(os.Stderr, "\033[1;31m[ERROR]\033[0m ", log.Ldate|log.Ltime|log.Llongfile)}
}
