package postgre

import (
	"github.com/jinzhu/gorm"
	"ooad-support/model"
	"ooad-support/repository"
)

func NewCourseRepository(tableName string, db *gorm.DB) repository.ICourseRepository {
	return &courseRepositoryIml{
		baseRepository{
			DB:    db,
			Table: db.Table(tableName),
		},
	}
}

type courseRepositoryIml struct {
	baseRepository
}

func (c *courseRepositoryIml) GetListCourseOffering(query *repository.CourseQuery) (total uint, data []*model.CourseOffering, err error) {
	data = []*model.CourseOffering{}
	err = c.Table.Preload("Teacher").Scopes(TeacherCode(query.TeacherCode)).Scopes(StudentCode(query.StudentCode)).Find(&data).Error
	return
}

func (c *courseRepositoryIml) GetStudentInCourseOffering(query *repository.CourseQuery) (total uint, data []*model.StudentCourseResponse, err error) {
	data = []*model.StudentCourseResponse{}
	err = c.DB.Select("student_courses.student_code as student_code,students.name as name ,students.school_year as school_year ,student_courses.total as total ,teams.id as team_id,teams.name as team_name").
		Table("student_courses").
		Joins("join students  on student_courses.student_code =students .code").
		Joins(" LEFT join team_students on students.code = team_students.student_code ").
		Joins("LEFT join teams on teams.id = team_students.team_id").Find(&data).Error
	return 0, data, err
}
func StudentCode(code string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if code!= ""{
			return db.Joins("join student_courses on student_courses.course_offering_code =course_offerings.code").Where("student_courses.student_code = ?", code)
		}
		return db
	}
}
func TeacherCode(code string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if code!= ""{
			return db.Where("teacher_code = ?", code)
		}
		return db

	}
}
