package postgre

import (
	"ooad-support/infrastructure"

	"github.com/jinzhu/gorm"
)

type baseRepository struct {
	DB    *gorm.DB
	Table *gorm.DB
	infrastructure.Logger
}

func (b *baseRepository) CreateOne(object interface{}) (err error) {
	return b.Table.Create(object).Error
}
func (b *baseRepository) UpdateOne(id string, val interface{}, object interface{}) error {
	return b.Table.Where(id+"=(?)", val).Update(object).Error
}
func (b *baseRepository) FindOne(id string, val interface{}, object interface{}) error {
	return b.Table.Where(id+"?", val).First(object).Error
}
func NewBaseRepository(name string, DB *gorm.DB, ) baseRepository {
	baseRepo := baseRepository{
		DB:     DB,
		Table:  DB.Table(name),
		Logger: infrastructure.NewLogger(),
	}
	baseRepo.ErrLog.SetPrefix("\033[32m[REPOSITORY]\033[0m " + baseRepo.ErrLog.Prefix())
	baseRepo.InfoLog.SetPrefix("\033[32m[REPOSITORY]\033[0m " + baseRepo.InfoLog.Prefix())
	return baseRepo
}
