package postgre

import (
	"github.com/jinzhu/gorm"
	"log"
	"ooad-support/model"
	"ooad-support/repository"
)

type groupRepositoryIml struct {
	baseRepository
}

func (g *groupRepositoryIml) GetListGroup(query *repository.GroupQuery) (total uint, records []*model.Team, err error) {
	records = []*model.Team{}
	err = g.Table.Order("id desc").Where("course_offering_code = ?", query.CourseCode).Preload("Members").Find(&records).Error
	return 0, records, err
}

func (g *groupRepositoryIml) CreateGroup(group *model.Team) (*model.Team, error) {
	// create group
	err := g.DB.Create(group).Error
	if err != nil {
		return nil, err
	}
	a := model.TeamStudent{
		TeamID:      group.ID,
		StudentCode: group.LeaderCode,
		CourseCode:  group.CourseOfferingCode,
		Status:      true,
	}
	err = g.DB.Create(&a).Error
	return group, err
}

func (g *groupRepositoryIml) IsNotStudentInGroup(studentCode string, courseCode string) (bool, error) {
	a := model.TeamStudent{}

	err := g.DB.Model(model.TeamStudent{}).Where("student_code = ? and course_code = ?", studentCode, courseCode).First(&a).Error
	if err == gorm.ErrRecordNotFound {
		return true, nil
	}
	log.Print(err)
	return false, err
}

func NewGroupRepository(tableName string, db *gorm.DB) repository.IGroupRepository {
	return &groupRepositoryIml{
		baseRepository{
			DB:    db,
			Table: db.Table(tableName),
		},
	}
}

