package postgre

import (
	"github.com/jinzhu/gorm"
	"ooad-support/model"
	"ooad-support/repository"
)

type topicRepositoryIml struct {
	baseRepository
}

func (t *topicRepositoryIml) GetListProject(query repository.TopicQuery) (total uint, records []*model.Project, err error) {
	records = []*model.Project{}
	err = t.Table.Order("id desc").Scopes(CourseCode(query.CourseOfferingCode)).Preload("OwnedByTeam").Find(&records).Error
	return 0, records, err
}

func NewTopicRepository(tableName string, db *gorm.DB) repository.ITopicRepository {
	return &topicRepositoryIml{
		baseRepository{
			DB:    db,
			Table: db.Table(tableName),
		},
	}
}
func CourseCode(code string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if code!= ""{
			return db.Where("course_code = ?", code)
		}
		return db

	}
}