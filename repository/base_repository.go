package repository

type BaseRepository interface {
	CreateOne(object interface{}) (err error)
	UpdateOne(id string, val interface{}, object interface{}) error
	FindOne(id string, val interface{}, object interface{}) error
}
