package repository

import "ooad-support/model"

type GroupQuery struct {
	CourseCode string
}
type IGroupRepository interface {
	BaseRepository
	IsNotStudentInGroup(studentCode string, courseCode string) (bool, error)
	CreateGroup(group *model.Team) (*model.Team, error)
	GetListGroup(query *GroupQuery) (total uint, records []*model.Team, err error)
}
