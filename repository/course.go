package repository

import "ooad-support/model"

type CourseQuery struct {
	CourseCode  string
	StudentCode string
	TeacherCode string
}
type ICourseRepository interface {
	GetListCourseOffering(query *CourseQuery) (total uint, data []*model.CourseOffering, err error)
	GetStudentInCourseOffering(query *CourseQuery) (total uint, data []*model.StudentCourseResponse, err error)
	BaseRepository
}
