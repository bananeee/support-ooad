package repository

import "ooad-support/model"

type TopicQuery struct {
	CourseOfferingCode string
}
type ITopicRepository interface {
	GetListProject(TopicQuery) (total uint, records []*model.Project, err error)
	BaseRepository
}
