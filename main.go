package main

import (
	"ooad-support/api"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	// Echo instance
	go func() {
		s := make(chan os.Signal, 1)
		signal.Notify(s, syscall.SIGQUIT)
		<-s
		panic("give me the stack")
	}()
	//migration.CreateTable()
	api.HandleHttpServer(":3000")

}
