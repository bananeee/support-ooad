FROM 149.28.157.34:5001/go-builder:latest AS builder

RUN apk add bash ca-certificates git gcc g++ libc-dev

RUN mkdir -p /projects/echo-template

WORKDIR /projects/echo-template
ENV GO111MODULE=on
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN ls -lha

# build
RUN swag init
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o echo-template



# Make small image for running this payload
FROM alpine:latest

MAINTAINER Tuan Anh <ptanh@anvita.com.vn>

RUN apk add --no-cache tzdata
RUN apk update && apk add --no-cache ca-certificates

WORKDIR /usr/local/bin

COPY --from=builder /projects/echo-template/echo-template /usr/local/bin/

CMD ["./echo-template"]