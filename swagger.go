package main 
 
// @Summary  GetGroupInCourse
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param course_code path string false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/courses/{course_code}/groups [GET]
func DefaultFunction0()  {}
 
 
 
// @Summary  CreateGroup
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.CreateGroupPayload true "information" 
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/groups [POST]
func DefaultFunction1()  {}
 
 
 
// @Summary  RejectMemberInGroup
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.RejectMemberInGroupPayload true "information" 
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/groups/reject/members/  [PUT]
func DefaultFunction2()  {}
 
 
 
// @Summary  GetStudentInCourse
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param course_code path string false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/courses/{course_code}/students [GET]
func DefaultFunction3()  {}
 
 
 
// @Summary  GetMyCourse
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/courses/me [GET]
func DefaultFunction4()  {}
 
 
 
// @Summary  GetListGroup
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param course_code query string false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/groups [GET]
func DefaultFunction5()  {}
 
 
 
// @Summary  UpdateGroup
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.UpdateGroupPayload true "information" 
// @Param group_id path integer false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/groups/{group_id} [PUT]
func DefaultFunction6()  {}
 
 
 
// @Summary  AcceptMemberInGroup
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.AcceptMemberInGroupPayload true "information" 
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/groups/accept/members [PUT]
func DefaultFunction7()  {}
 
 
 
// @Summary  GetListProject
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param course_code query string false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/projects [GET]
func DefaultFunction8()  {}
 
 
 
// @Summary  CreateProject
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.CreateProjectPayload true "information" 
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/projects [POST]
func DefaultFunction9()  {}
 
 
 
// @Summary  UpdateProject
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param payload body payload.UpdateProjectPayload true "information" 
// @Param project_id path integer false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/projects/{project_id} [PUT]
func DefaultFunction10()  {}
 
 
 
// @Summary  GetProjectInCourse
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param course_code path string false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/courses/{course_code}/projects [GET]
func DefaultFunction11()  {}
 
 
 
// @Summary  GetProject
// @Description ádasd
// @Id
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param project_id path integer false "count"   
// @Success 200 {object} response.Response  "desc" 
// @Router /api/v1/ooad-support/projects/{project_id} [GET]
func DefaultFunction12()  {}
 
 
 
