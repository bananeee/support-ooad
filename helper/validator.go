package helper

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"ooad-support/model"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}
func NewCustomValidator() *CustomValidator {
	return &CustomValidator{
		validator: validator.New(),
	}
}
func GetUser(u string) (user *model.User, err error) {
	user= new(model.User)
	in:=[]byte(u)
	err=json.Unmarshal(in,user)
	return
}
