package helper

import (
	"regexp"
	"runtime"
)
var RE_stripFnPreamble = regexp.MustCompile(`^.*\.(.*)$`)
func GetNameFunctionCall() string {
	fnName := "<unknown>"
	pc, _, _, ok := runtime.Caller(1)
	funcName:=runtime.FuncForPC(pc).Name()
	if ok {
		fnName = RE_stripFnPreamble.ReplaceAllString(funcName, "$1")
	}
	return fnName
}
