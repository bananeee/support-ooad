package api

import (
	"encoding/json"
	"io/ioutil"
	"log"
	controller "ooad-support/controller"
	_ "ooad-support/docs"
	"ooad-support/helper"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
)

const basePath = "/api/v1/ooad-support"

func RootRouter() *echo.Echo {

	root := echo.New()
	root.Validator = helper.NewCustomValidator()
	//swagger

	// main router
	root.Use(middleware.Logger())
	mainRouter := root.Group(basePath)
	courseController := controller.NewCourseController()
	mainRouter.GET("/courses/:course_code/students", courseController.GetStudentInCourse)
	mainRouter.GET("/courses/:course_code/groups", courseController.GetGroupInCourse)
	mainRouter.GET("/courses/:course_code/projects", courseController.GetProjectInCourse)
	mainRouter.GET("/courses/me", courseController.GetMyCourse)
	groupController := controller.NewGroupController()
	mainRouter.GET("/groups", groupController.GetListGroup)
	mainRouter.POST("/groups", groupController.CreateGroup)
	mainRouter.PUT("/groups/:group_id", groupController.UpdateGroup)
	mainRouter.PUT("/groups/accept/members/", groupController.AcceptMemberInGroup)
	mainRouter.PUT("/groups/reject/members/ ", groupController.RejectMemberInGroup)
	projectController := controller.NewProjectController()
	mainRouter.GET("/projects", projectController.GetListProject)
	mainRouter.POST("/projects", projectController.CreateProject)
	mainRouter.PUT("/projects/:project_id", projectController.UpdateProject)
	mainRouter.GET("/projects/:project_id", projectController.GetProject)
	data, err := json.MarshalIndent(root.Routes(), "", "  ")
	if err != nil {
	}
	ioutil.WriteFile("routes.json", data, 0644)
	root.GET(basePath+"/swagger/*", echoSwagger.WrapHandler)
	return root
}
func HandleHttpServer(port string) {
	if err := RootRouter().Start(port); err != nil {
		log.Panic(err)
	}
}
