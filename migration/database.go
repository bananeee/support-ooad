package migration

import (
	"math/rand"
	"ooad-support/infrastructure"
	"ooad-support/model"
	"ooad-support/version_control"
	"strconv"
)

func CreateTeacher() {
	for i := 0; i < 6; i++ {
		teacher := model.Teacher{
			Code:   "10000001" + strconv.Itoa(i),
			Name:   "giangvien" + strconv.Itoa(i),
			Avatar: "",
		}
		version_control.GetPostgreConnection().Model(&teacher).Create(&teacher)
	}

}
func CreateCourseOffering() {

	for i := 0; i < 9; i++ {
		course := model.CourseOffering{
			Code:        "2020_" + strconv.Itoa(rand.Intn(2)) + "_INT 3110" + strconv.Itoa(i),
			Name:        "Phân tích thiết kế hướng đối tượng",
			Location:    "P.304,GD2",
			StartLesson: uint(i),
			EndLesson:   uint(i + 3),
			SchoolYear:  "2020-2021",
			TeacherCode: "10000001" + strconv.Itoa(i%6),
			Weekday:     "Thứ 2",
			Serial:      "học kì" + strconv.Itoa(rand.Intn(2)),
		}
		if i%3 == 0 {
			course.Status = "Hoàn thành"
		} else {
			course.Status = "Đang diễn ra"
		}
		version_control.GetPostgreConnection().Model(model.CourseOffering{}).Create(&course)
	}

}
func CreateStudent() {
	for i := 0; i < 20; i++ {
		student := model.Student{
			Code:       "170200" + strconv.Itoa(i),
			Name:       "sinhvien" + strconv.Itoa(i),
			SchoolYear: "K62 CQK",
			Avatar:     "",
		}
		version_control.GetPostgreConnection().Model(&model.Student{}).Create(&student)
	}
}
func CreateStudentInCourse() {
	course := model.CourseOffering{}
	err := version_control.GetPostgreConnection().First(&course).Error
	infrastructure.InfoLog.Print(err)
	for i := 0; i < 20; i++ {
		data := model.StudentCourse{
			StudentCode:        "170200" + strconv.Itoa(i),
			CourseOfferingCode: course.Code,
			Total:              0,
			Status:             true,
		}
		err := version_control.GetPostgreConnection().Create(&data).Error
		infrastructure.InfoLog.Print(err)
	}

}
//func CreateTeam() {
//	course := model.CourseOffering{}
//	err := infrastructure.GetPostgreConnection().First(&course).Error
//	infrastructure.InfoLog.Print(err)
//	for i := 0; i < 5; i++ {
//		data := model.Team{
//			Name:               "Nhóm" + strconv.Itoa(i),
//			LeaderCode:         "170200" + strconv.Itoa(i),
//			CourseOfferingCode: course.Code,
//			CourseOffering:     model.CourseOffering{},
//		}
//		err := infrastructure.GetPostgreConnection().Create(&data).Error
//		data2 := model.TeamStudent{
//			TeamID:      data.ID,
//			StudentCode: "170200" + strconv.Itoa(i),
//			Status:      true,
//		}
//		err = infrastructure.GetPostgreConnection().Create(&data2).Error
//		infrastructure.InfoLog.Print(err)
//	}
//
//}
//func CreateProject() {
//
//	for i := 0; i < 5; i++ {
//		team := model.Team{}
//		err := infrastructure.GetPostgreConnection().Table("teams").Where("name =?", "Nhóm"+strconv.Itoa(i)).Find(&team)
//		infrastructure.ErrLog.Print(err)
//		data := model.Project{}
//		if i == 1 {
//			data = model.Project{
//				Name:             "Hệ thống hỗ trợ môn phân tích thiết kế hướng đối tượng",
//				ShortDescription: "Hệ thống hỗ trợ môn phân tích thiết kế hướng đối tượng",
//				Description:      "",
//				State:            "accept",
//				CreatedBy:        "giangvien1",
//				OwnedByTeamID:    team.ID,
//				Status:           true,
//			}
//		}
//		if i == 2 {
//			data = model.Project{
//				Name:             "Hệ thống Vận chuyển",
//				ShortDescription: "Hệ thống Vận chuyển cho xe khách",
//				Description:      "",
//				State:            "accept",
//				CreatedBy:        "sinhvien1",
//				OwnedByTeamID:    team.ID,
//				Status:           true,
//			}
//		}
//		if i == 3 {
//			data = model.Project{
//				Name:             "Hệ thống thư viện",
//				ShortDescription: "Hệ thống thư viện",
//				Description:      "",
//				State:            "wait",
//				CreatedBy:        "sinhvien2",
//				OwnedByTeamID:    team.ID,
//				Status:           true,
//			}
//		}
//		if i == 4 {
//			data = model.Project{
//				Name:             "Hệ thống gửi xe ",
//				ShortDescription: "Hệ thống gửi xe",
//				Description:      "",
//				State:            "cancel",
//				CreatedBy:        "",
//				OwnedByTeamID:    team.ID,
//				Status:           true,
//			}
//		}
//
//		_ = infrastructure.GetPostgreConnection().Create(&data).Error
//	}
//
//}
func CreateTable() {
	// drop foreign key
	version_control.GetPostgreConnection().Model(&model.Project{}).RemoveForeignKey("owned_by_team_id", "teams(id)")
	version_control.GetPostgreConnection().Model(&model.CourseOffering{}).RemoveForeignKey("teacher_code", "teachers(code)")
	version_control.GetPostgreConnection().Model(&model.StudentCourse{}).RemoveForeignKey("student_code", "students(code)")
	version_control.GetPostgreConnection().Model(&model.StudentCourse{}).RemoveForeignKey("course_offering_code", "course_offerings(code)")
	version_control.GetPostgreConnection().Model(&model.TeamStudent{}).RemoveForeignKey("team_id", "teams(id)")
	version_control.GetPostgreConnection().Model(&model.TeamStudent{}).RemoveForeignKey("student_code", "students(code)")
	version_control.GetPostgreConnection().Model(&model.Team{}).RemoveForeignKey("course_offering_code", "course_offerings(code)")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).RemoveForeignKey("student_code", "students(code)")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).RemoveForeignKey("use_case_id", "use_cases(id)")
	version_control.GetPostgreConnection().Model(&model.UseCase{}).RemoveForeignKey("project_id", "projects(id)")
	version_control.GetPostgreConnection().Model(&model.Mark{}).RemoveForeignKey("student_code", "students(code)")
	version_control.GetPostgreConnection().Model(&model.Mark{}).RemoveForeignKey("use_case_slice_id", "use_case_slices(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCase{}).RemoveForeignKey("from_use_case_id", "use_cases(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCase{}).RemoveForeignKey("to_use_case_id", "use_cases(id)")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).RemoveForeignKey("from_flow_step_id", "flow_steps(id)")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).RemoveForeignKey("to_flow_step_id", "flow_steps(id)")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).RemoveForeignKey("use_case_story_id", "use_case_stories(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCaseStory{}).RemoveForeignKey("use_case_story_id", "use_case_stories(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCaseStory{}).RemoveForeignKey("use_case_id", "use_cases(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToFlowStep{}).RemoveForeignKey("to_use_case_id", "use_cases(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseToFlowStep{}).RemoveForeignKey("from_flow_step_id", "flow_steps(id)")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).RemoveForeignKey("iteration_id", "iterations(id)")

	// drop table
	version_control.GetPostgreConnection().DropTable(&model.CourseOffering{}, &model.Teacher{}, &model.Student{}, &model.Team{}, &model.StudentCourse{},
		model.TeamStudent{}, &model.Project{}, &model.UseCase{}, &model.UseCaseSlice{}, &model.Mark{}, &model.Iteration{}, &model.FlowStep{}, &model.UseCaseToUseCase{},
		&model.FlowStepToFlowStep{}, &model.UseCaseToFlowStep{}, &model.UseCaseStory{}, &model.UseCaseToUseCaseStory{})

	// create table
	version_control.GetPostgreConnection().CreateTable(&model.CourseOffering{}, &model.Teacher{}, &model.Student{}, &model.Team{}, &model.StudentCourse{},
		model.TeamStudent{}, &model.Project{}, &model.UseCase{}, &model.UseCaseSlice{}, &model.Mark{}, &model.Iteration{}, &model.FlowStep{}, &model.UseCaseToUseCase{},
		&model.FlowStepToFlowStep{}, &model.UseCaseToFlowStep{}, &model.UseCaseStory{}, &model.UseCaseToUseCaseStory{})

	// create  foreign key
	version_control.GetPostgreConnection().Model(&model.CourseOffering{}).AddForeignKey("teacher_code", "teachers(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.StudentCourse{}).AddForeignKey("student_code", "students(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.StudentCourse{}).AddForeignKey("course_offering_code", "course_offerings(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.TeamStudent{}).AddForeignKey("team_id", "teams(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.TeamStudent{}).AddForeignKey("student_code", "students(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.Team{}).AddForeignKey("course_offering_code", "course_offerings(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.Project{}).AddForeignKey("owned_by_team_id", "teams(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCase{}).AddForeignKey("project_id", "projects(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).AddForeignKey("use_case_id", "use_cases(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).AddForeignKey("student_code", "students(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.Mark{}).AddForeignKey("student_code", "students(code)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.Mark{}).AddForeignKey("use_case_slice_id", "use_case_slices(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCase{}).AddForeignKey("from_use_case_id", "use_cases(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCase{}).AddForeignKey("to_use_case_id", "use_cases(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).AddForeignKey("from_flow_step_id", "flow_steps(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).AddForeignKey("to_flow_step_id", "flow_steps(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.FlowStepToFlowStep{}).AddForeignKey("use_case_story_id", "use_case_stories(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCaseStory{}).AddForeignKey("use_case_story_id", "use_case_stories(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToUseCaseStory{}).AddForeignKey("use_case_id", "use_cases(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToFlowStep{}).AddForeignKey("to_use_case_id", "use_cases(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseToFlowStep{}).AddForeignKey("from_flow_step_id", "flow_steps(id)", "RESTRICT", "RESTRICT")
	version_control.GetPostgreConnection().Model(&model.UseCaseSlice{}).AddForeignKey("iteration_id", "iterations(id)", "RESTRICT", "RESTRICT")

	CreateTeacher()
	CreateCourseOffering()
	CreateStudent()
	CreateStudentInCourse()
	//CreateTeam()
	//CreateProject()

}
